#------------------------------------------------------------------------
#
# File  : Makefile (top level make file for E and its libraries)
#
# Author: Stephan Schulz
#
# Top level make file. Check Makefile.vars for system-dependend tool
# selection and compilation options. 
#
# Changes
#
# <1> Sun Jul  6 22:55:11 MET DST 1997
#     New
# <2> Mon Jan 12 14:05:24 MET 1998
#     Extended for DOC directory
# <3> Fri Mar 13 17:09:13 MET 1998
#     Extended for CLAUSES directory
#
#------------------------------------------------------------------------

.PHONY: all depend cleandist distrib fulldistrib top tags tools rebuild install test config remake documentation E

include Makefile.vars

# Project specific variables

PROJECT = E

LIBS     = BASICS INOUT TERMS ORDERINGS CLAUSES PROPOSITIONAL LEARN \
           ANALYSIS PCL2 HEURISTICS CONTROL
HEADERS  = $(LIBS) EXTERNAL
CODE     = $(LIBS) PROVER TEST SIMPLE_APPS EXTERNAL
PARTS    = $(CODE) DOC

all: E


# Generate dependencies

depend: ;
# automatic now

# Remove all automatically generated files


clean:
	for subdir in $(PARTS); do\
	    $(MAKE) -C $$subdir clean ; \
	done;

cleandist: clean
	rm -f *~ */*~ Makefile.cfg */*.d


# Build a distribution

distrib: cleandist
	@echo "Did you think about: "
	@echo " - Changing the bibliographies to local version"
	@echo "    ??? "
	@cp etc/PROPRIETARY etc/NO_DISTRIB
	@cd ..; find $(PROJECT) -name "CVS" -print >> $(PROJECT)/etc/NO_DISTRIB;\
         $(TAR) cfX - $(PROJECT)/etc/NO_DISTRIB $(PROJECT) |$(GZIP) - -c > $(PROJECT).tgz

# Include proprietary code not part of the GPL'ed version, 
# as well as CVS subdirecctories

fulldistrib: cleandist
	@echo "Warning: You are building a full archive!"
	@echo "Did you remember to increase the dev version number and commit to CVS?"
	cd ..; $(TAR) cf - $(PROJECT)|$(GZIP) - -c > $(PROJECT)_FULL.tgz

# Make all library parts

top: E

# Create symbolic links

links: remove_links
	cd include;\
	for subdir in $(HEADERS); do\
	   for file in ../$$subdir/*.h; do\
	     $(LN) $$file .;\
	   done;\
	done;
	cd lib;\
	for subdir in $(LIBS); do\
           $(LN) ../$$subdir/$$subdir.a .;\
	done;

tags: 
	etags */*.c */*.h
	cd PYTHON; make tags

tools:
	$(MAKE) -C development_tools tools
	$(MAKE) -C PYTHON tools

# Rebuilding from scratch

rebuild:
	echo 'Rebuilding with debug options $(DEBUGFLAGS)'	
	$(MAKE) clean
	$(MAKE) config
	$(MAKE) all

# Configure the whole package

config: ;
# Tools target modifies local source files, which is unsuitable for
# automatic packaging.
#	$(MAKE) tools


# Configure and copy executables to the installation directory

# Old eproof config - I hope it now runs with /bin/sh, which is 
# guaranteed to be where it belongs. Kept as a historical reference.
# 	@echo "#!"`which bash`" -f" > tmpfile
#	@echo "" >> tmpfile
#	@echo "EXECPATH=$(EXECPATH)" >> tmpfile
#	@awk '{count++; if(count >= 4){print}}' PROVER/eproof >> tmpfile
#	@mv tmpfile PROVER/eproof


# $(DESTDIR) is used for Debian packaging - it is a build
# directory into which everything is copied.
# Normally it expands to just an empty string.
install: E
	sh -c 'mkdir -p $(DESTDIR)$(EXECPATH)'
	sh -c 'cp PROVER/eprover $(DESTDIR)$(EXECPATH)'
	sh -c 'cp PROVER/epclextract $(DESTDIR)$(EXECPATH)'
	sh -c 'sed -e "/^EXECPATH=.*/s|.*|EXECPATH=$(EXECPATH)|" PROVER/eproof > $(DESTDIR)$(EXECPATH)/eproof'
	sh -c 'chmod 0755 $(DESTDIR)$(EXECPATH)/eproof'
	sh -c 'cp PROVER/eground $(DESTDIR)$(EXECPATH)'	

test: E
	PROVER/eprover --output-level=0 --tptp-format -xAuto EXAMPLE_PROBLEMS/TPTP/BOO006-1+rm_eq_rstfp.tptp | grep -q 'Unsatisfiable'

# Also remake documentation

remake: config rebuild documentation

documentation:
	$(MAKE) -C DOC all

# Build the single libraries

E:
	@if [ "$(EXECPATH)" = "" ] ; then \
	    echo ">>>> EXECPATH undefined, please run './configure' first!" ; \
	    exit 1 ; \
	fi
	for subdir in $(CODE); do\
	    $(MAKE) -C $$subdir all ; \
	done;


