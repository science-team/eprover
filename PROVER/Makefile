#------------------------------------------------------------------------
#
# File  : Makefile for E prover
#
# Author: Stephan Schulz
#
# Changes
#
# <1> Tue Jun  9 01:29:39 MET DST 1998
#     New
#
#------------------------------------------------------------------------

include ../Makefile.vars

# Project specific variables

PROJECT = eprover classify_problem termprops\
          direct_examples epclanalyse epclextract checkproof eground\
          edpll epcllemma\
          ekb_create ekb_insert ekb_ginsert ekb_delete tsm_classify
LIB     = $(PROJECT)
all: $(LIB)


# Remove all automatically generated files

clean:
	@touch does_exist.o $(PROJECT)
	@rm *.o $(PROJECT)
	@echo Removed compiled files

# Services (provided by the master Makefile)

include ../Makefile.services

# Build the programs

EPROVER = eprover.o ../CONTROL/CONTROL.a ../HEURISTICS/HEURISTICS.a\
            ../LEARN/LEARN.a\
            ../CLAUSES/CLAUSES.a ../ORDERINGS/ORDERINGS.a ../TERMS/TERMS.a\
            ../INOUT/INOUT.a ../BASICS/BASICS.a

eprover: $(EPROVER)
	$(LD) -o eprover $(EPROVER)

EGROUND = eground.o ../HEURISTICS/HEURISTICS.a\
            ../CLAUSES/CLAUSES.a ../ORDERINGS/ORDERINGS.a ../TERMS/TERMS.a\
            ../INOUT/INOUT.a ../BASICS/BASICS.a

eground: $(EGROUND)
	$(LD) -o eground $(EGROUND)

EDPLL = edpll.o ../PROPOSITIONAL/PROPOSITIONAL.a ../CLAUSES/CLAUSES.a\
        ../ORDERINGS/ORDERINGS.a ../TERMS/TERMS.a ../INOUT/INOUT.a\
	../BASICS/BASICS.a 

edpll: $(EDPLL)
	$(LD) -o edpll $(EDPLL)

CLASSIFY = classify_problem.o ../HEURISTICS/HEURISTICS.a\
            ../CLAUSES/CLAUSES.a ../ORDERINGS/ORDERINGS.a ../TERMS/TERMS.a\
            ../INOUT/INOUT.a ../BASICS/BASICS.a

classify_problem: $(CLASSIFY)
	$(LD) -o classify_problem $(CLASSIFY)

TERMPROPS = termprops.o ../TERMS/TERMS.a\
            ../INOUT/INOUT.a ../BASICS/BASICS.a

termprops: $(TERMPROPS)
	$(LD) -o termprops $(TERMPROPS)

DIRECT_EXAMPLES = direct_examples.o \
            ../PCL2/PCL2.a ../CLAUSES/CLAUSES.a ../ORDERINGS/ORDERINGS.a \
            ../TERMS/TERMS.a ../INOUT/INOUT.a ../BASICS/BASICS.a

direct_examples: $(DIRECT_EXAMPLES)
	$(LD) -o direct_examples $(DIRECT_EXAMPLES)


EPCLANALYSE = epclanalyse.o \
        ../PCL2/PCL2.a ../HEURISTICS/HEURISTICS.a ../CLAUSES/CLAUSES.a ../ORDERINGS/ORDERINGS.a \
        ../TERMS/TERMS.a ../INOUT/INOUT.a ../BASICS/BASICS.a

epclanalyse: $(EPCLANALYSE)
	$(LD) -o epclanalyse $(EPCLANALYSE)


EPCLLEMMA = epcllemma.o \
        ../PCL2/PCL2.a ../HEURISTICS/HEURISTICS.a ../CLAUSES/CLAUSES.a ../ORDERINGS/ORDERINGS.a \
        ../TERMS/TERMS.a ../INOUT/INOUT.a ../BASICS/BASICS.a

epcllemma: $(EPCLLEMMA)
	$(LD) -o epcllemma $(EPCLLEMMA)


EPCLEXTRACT = epclextract.o \
        ../PCL2/PCL2.a ../HEURISTICS/HEURISTICS.a ../CLAUSES/CLAUSES.a ../ORDERINGS/ORDERINGS.a \
        ../TERMS/TERMS.a ../INOUT/INOUT.a ../BASICS/BASICS.a

epclextract: $(EPCLEXTRACT)
	$(LD) -o epclextract $(EPCLEXTRACT)

CHECKPROOF = checkproof.o \
        ../PCL2/PCL2.a ../CLAUSES/CLAUSES.a ../ORDERINGS/ORDERINGS.a \
        ../TERMS/TERMS.a ../INOUT/INOUT.a ../BASICS/BASICS.a

checkproof: $(CHECKPROOF)
	$(LD) -o checkproof $(CHECKPROOF)

EKB_CREATE = ekb_create.o \
            ../LEARN/LEARN.a ../ANALYSIS/ANALYSIS.a ../CLAUSES/CLAUSES.a \
            ../ORDERINGS/ORDERINGS.a \
            ../TERMS/TERMS.a ../INOUT/INOUT.a ../BASICS/BASICS.a

ekb_create: $(EKB_CREATE)
	$(LD) -o ekb_create $(EKB_CREATE)

EKB_INSERT = ekb_insert.o \
            ../LEARN/LEARN.a ../ANALYSIS/ANALYSIS.a ../CLAUSES/CLAUSES.a \
            ../ORDERINGS/ORDERINGS.a \
            ../TERMS/TERMS.a ../INOUT/INOUT.a ../BASICS/BASICS.a

ekb_insert: $(EKB_INSERT)
	$(LD) -o ekb_insert $(EKB_INSERT)

EKB_GINSERT = ekb_ginsert.o \
	    ../PCL2/PCL2.a ../LEARN/LEARN.a ../ANALYSIS/ANALYSIS.a ../CLAUSES/CLAUSES.a \
            ../ORDERINGS/ORDERINGS.a \
            ../TERMS/TERMS.a ../INOUT/INOUT.a ../BASICS/BASICS.a

ekb_ginsert: $(EKB_GINSERT)
	$(LD) -o ekb_ginsert $(EKB_GINSERT)

EKB_DELETE = ekb_delete.o \
            ../LEARN/LEARN.a ../ANALYSIS/ANALYSIS.a ../CLAUSES/CLAUSES.a \
            ../ORDERINGS/ORDERINGS.a \
            ../TERMS/TERMS.a ../INOUT/INOUT.a ../BASICS/BASICS.a

ekb_delete: $(EKB_DELETE)
	$(LD) -o ekb_delete $(EKB_DELETE)

TSM_CLASSIFY = tsm_classify.o \
            ../LEARN/LEARN.a ../ANALYSIS/ANALYSIS.a ../CLAUSES/CLAUSES.a \
            ../ORDERINGS/ORDERINGS.a \
            ../TERMS/TERMS.a ../INOUT/INOUT.a ../BASICS/BASICS.a

tsm_classify: $(TSM_CLASSIFY)
	$(LD) -o tsm_classify $(TSM_CLASSIFY)






