\contentsline {section}{\numberline {1}Introduction}{2}
\contentsline {section}{\numberline {2}Getting Started}{2}
\contentsline {section}{\numberline {3}Calculus and Proof Procedure}{3}
\contentsline {subsection}{\numberline {3.1}Calculus}{3}
\contentsline {subsection}{\numberline {3.2}Proof Procedure}{7}
\contentsline {section}{\numberline {4}Usage}{9}
\contentsline {subsection}{\numberline {4.1}Search Control Heuristics}{9}
\contentsline {subsubsection}{\numberline {4.1.1}Priority functions}{9}
\contentsline {subsubsection}{\numberline {4.1.2}Generic Weight Functions}{12}
\contentsline {subsubsection}{\numberline {4.1.3}Clause Evaluation Functions}{13}
\contentsline {subsubsection}{\numberline {4.1.4}Heuristics}{14}
\contentsline {subsection}{\numberline {4.2}Term Orderings}{14}
\contentsline {subsubsection}{\numberline {4.2.1}Precedence Generation Schemes}{16}
\contentsline {subsubsection}{\numberline {4.2.2}Weight Generation Schemes}{17}
\contentsline {subsection}{\numberline {4.3}Literal Selection Strategies}{18}
\contentsline {subsection}{\numberline {4.4}The Watchlist Feature}{20}
\contentsline {subsection}{\numberline {4.5}Learning Clause Evaluation Functions}{21}
\contentsline {subsubsection}{\numberline {4.5.1}Creating Knowledge Bases}{21}
\contentsline {subsubsection}{\numberline {4.5.2}Populating Knowledge Bases}{21}
\contentsline {subsubsection}{\numberline {4.5.3}Using Learned Knowledge}{21}
\contentsline {subsection}{\numberline {4.6}Other Options}{22}
\contentsline {section}{\numberline {5}Input Language}{22}
\contentsline {subsection}{\numberline {5.1}LOP}{22}
\contentsline {subsection}{\numberline {5.2}TPTP Format}{22}
\contentsline {section}{\numberline {6}Output\ldots or how to interpret what you see}{23}
\contentsline {subsection}{\numberline {6.1}The Bare Essentials}{23}
\contentsline {subsection}{\numberline {6.2}Impressing your Friends}{25}
\contentsline {subsection}{\numberline {6.3}Detailed Reporting}{25}
\contentsline {subsection}{\numberline {6.4}Requesting Specific Results}{25}
\contentsline {section}{\numberline {A}License}{27}
